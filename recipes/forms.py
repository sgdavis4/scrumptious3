from django.forms import ModelForm
from recipes.models import Recipe_Class

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe_Class
        fields = [
            "title",
            "picture",
            "description",
        ]

class EditForm(ModelForm):
    class Meta:
        model = Recipe_Class
        fields = [
            "title",
            "picture",
            "description",
        ]
