from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe_Class
from recipes.forms import RecipeForm, EditForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe_Class, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes = Recipe_Class.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


@login_required
def create_recipe(request):
    if request.method =="POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()

    context = {
        "form":form,
        }
    return render(request, "recipes/create.html", context)


def edit_recipe(request,id):
    recipe = get_object_or_404(Recipe_Class, id=id)
    if request.method == "POST":
        edit_form = EditForm(request.POST, instance = recipe)
        if edit_form.is_valid():
            edit_form.save()
            return redirect("show_recipe",id=id)

    else:
        edit_form = EditForm(instance=recipe)

    context = {
        "edit_form" : edit_form,
        "recipe_object" : recipe
    }
    return render(request, "recipes/edit.html", context)
