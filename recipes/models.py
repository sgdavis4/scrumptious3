from django.db import models
from django.conf import settings


# Create your models here.
class Recipe_Class(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self) -> str:
        return self.title


# Recipe Step Model
class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe_Class,
        related_name="step",
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ["step_number"]


# Ingredeient Model
class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe_Class,
        related_name="ingredients",
        on_delete=models.CASCADE
    )
